package top.chenfu.boot.aop.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Auther chenfu
 * @2019/12/18 20:38
 * @Desc
 */
@Aspect
@Order(value = 1)
@Component
public class Aspect1 {

    @Before(value = "@annotation(top.chenfu.boot.aop.anno.AopAnno1)")
    public void before() {
        System.out.println("====before====");
    }

    @After(value = "@annotation(top.chenfu.boot.aop.anno.AopAnno1)")
    public void after() {
        System.out.println("====after====");
    }

}
