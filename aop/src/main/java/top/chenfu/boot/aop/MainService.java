package top.chenfu.boot.aop;

import org.springframework.stereotype.Service;
import top.chenfu.boot.aop.anno.AopAnno1;

/**
 * @Auther chenfu
 * @2019/12/18 20:41
 * @Desc
 */
@Service
public class MainService {

    @AopAnno1
    public void service(String msg) {
        System.out.println("service====" + msg);
    }

    @AopAnno1
    public void main(String msg) {
        System.out.println("main====" + msg);
    }

    /**
     * 演示类内部的自调用，不会出现AOP【service和main方法】
     *
     * @param msg
     */
    @AopAnno1
    public void servicePlus(String msg) {
//        此处直接打印service方法和main方法内的内容，不会在service和main前后增加before和after的输出
        service(msg);
        main(msg);
        System.out.println("Over");
    }

}
