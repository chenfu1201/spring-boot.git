package top.chenfu.boot.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @Auther chenfu
 * @2019/12/18 20:35
 * @Desc
 */
@SpringBootApplication
public class AopApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext app = SpringApplication.run(AopApp.class, args);
        MainService mainService = (MainService) app.getBean("mainService");
        mainService.service("service");
        System.out.println("============================================");
        mainService.main("main");
        System.out.println("============================================");
        mainService.servicePlus("servicePlus");
    }
}
