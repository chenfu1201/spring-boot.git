package top.chenfu.boot.auth.model;

import javax.persistence.*;

@Table(name = "menu")
public class Menu {
    /**
     * 菜单ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 访问URI
     */
    private String uri;

    /**
     * 菜单名称
     */
    @Column(name = "menu_name")
    private String menuName;

    /**
     * 菜单等级-0，1，2分别对应三级菜单
     */
    @Column(name = "menu_level")
    private Boolean menuLevel;

    /**
     * 注释
     */
    private String comment;

    /**
     * 获取菜单ID
     *
     * @return id - 菜单ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置菜单ID
     *
     * @param id 菜单ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取访问URI
     *
     * @return uri - 访问URI
     */
    public String getUri() {
        return uri;
    }

    /**
     * 设置访问URI
     *
     * @param uri 访问URI
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * 获取菜单名称
     *
     * @return menu_name - 菜单名称
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * 设置菜单名称
     *
     * @param menuName 菜单名称
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     * 获取菜单等级-0，1，2分别对应三级菜单
     *
     * @return menu_level - 菜单等级-0，1，2分别对应三级菜单
     */
    public Boolean getMenuLevel() {
        return menuLevel;
    }

    /**
     * 设置菜单等级-0，1，2分别对应三级菜单
     *
     * @param menuLevel 菜单等级-0，1，2分别对应三级菜单
     */
    public void setMenuLevel(Boolean menuLevel) {
        this.menuLevel = menuLevel;
    }

    /**
     * 获取注释
     *
     * @return comment - 注释
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置注释
     *
     * @param comment 注释
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
}