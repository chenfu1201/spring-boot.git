package top.chenfu.boot.auth.model;

import javax.persistence.*;

@Table(name = "user")
public class User {
    /**
     * 用户ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 删除标识，0是可用，1是已删除
     */
    @Column(name = "delete_flag")
    private Boolean deleteFlag;

    /**
     * 是否启用，0是启用，1是禁用
     */
    @Column(name = "is_enable")
    private Boolean isEnable;

    /**
     * 获取用户ID
     *
     * @return id - 用户ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置用户ID
     *
     * @param id 用户ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户名
     *
     * @return user_name - 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名
     *
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取删除标识，0是可用，1是已删除
     *
     * @return delete_flag - 删除标识，0是可用，1是已删除
     */
    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * 设置删除标识，0是可用，1是已删除
     *
     * @param deleteFlag 删除标识，0是可用，1是已删除
     */
    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 获取是否启用，0是启用，1是禁用
     *
     * @return is_enable - 是否启用，0是启用，1是禁用
     */
    public Boolean getIsEnable() {
        return isEnable;
    }

    /**
     * 设置是否启用，0是启用，1是禁用
     *
     * @param isEnable 是否启用，0是启用，1是禁用
     */
    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }
}