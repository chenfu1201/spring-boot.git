package top.chenfu.boot.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.Arrays;
import java.util.List;

@SpringBootConfiguration
public class InterceptorsConfig extends WebMvcConfigurationSupport {

    @Autowired
    private LoginInterceptor loginInterceptor;

    /**
     * 把静态资源集中整理
     *
     * @return List
     */
    private List<String> pathPatterns() {
        return Arrays.asList("/login", "/error", "/META-INF/**", "/swagger-ui.html", "/swagger-resources/**");
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor).addPathPatterns("/**")
                .excludePathPatterns(pathPatterns());
        super.addInterceptors(registry);
    }

    /**
     * 我们在访问http://localhost/swagger-ui.html时，这个swagger-ui.html相关的所有前端静态文件都在springfox-swagger-ui-2.4.0.jar里面
     * SpringBoot自动配置本身并不会把/swagger-ui.html这个路径映射到对应的目录META-INF/resources/下面。我们加上这个映射即可l
     *
     * @param registry
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry);
    }

}
