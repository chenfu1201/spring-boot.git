package top.chenfu.boot.auth.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserStatusEnum {

    /**
     * 作用看desc
     */
    ENABLE(false, "用户启用"),

    DISABLE(true, "状态禁用"),

    NO_DELETE(false, "用户未删除"),

    IS_DELETE(true, "用户已删除"),

    IS_PATCH(true, "部分更新");

    private boolean status;

    private String desc;

}
