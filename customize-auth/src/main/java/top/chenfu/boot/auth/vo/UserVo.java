package top.chenfu.boot.auth.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.chenfu.boot.auth.model.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "UserVo", description = "用户对象User")
public class UserVo extends User implements Serializable {

    private static final long serialVersionUID = -8334305506880272387L;

    @ApiModelProperty(value = "错误信息", name = "errorMsg")
    private String msg;

    @ApiModelProperty(value = "用户ID", name = "id", required = true)
//    @NotNull(message = "用户ID不能为null")
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名称", name = "userName", required = true)
    @NotBlank(message = "用户名信息不能为null或空串")
    private String userName;

    /**
     * 密码
     */
    @ApiModelProperty(value = "用户密码", name = "password", required = true, example = "123456")
    @NotBlank(message = "密码不能为null或空串")
    private String password;

    @ApiModelProperty(value = "响应码", name = "cc", hidden = true)
    private Integer code;

}
