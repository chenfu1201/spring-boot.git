package top.chenfu.boot.auth.dao;

import tk.mybatis.mapper.common.Mapper;
import top.chenfu.boot.auth.model.UserRole;

public interface UserRoleMapper extends Mapper<UserRole> {
}