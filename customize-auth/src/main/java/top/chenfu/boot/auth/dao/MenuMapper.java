package top.chenfu.boot.auth.dao;

import tk.mybatis.mapper.common.Mapper;
import top.chenfu.boot.auth.model.Menu;

public interface MenuMapper extends Mapper<Menu> {
}