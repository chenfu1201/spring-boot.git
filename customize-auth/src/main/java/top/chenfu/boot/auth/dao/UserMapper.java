package top.chenfu.boot.auth.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import top.chenfu.boot.auth.model.User;

@Repository
public interface UserMapper extends Mapper<User> {

    User login(@Param(value = "userName") String userName, @Param(value = "password") String password);

}