package top.chenfu.boot.auth.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import top.chenfu.boot.auth.util.Constant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@SpringBootConfiguration
public class Swagger2Config {

    /**
     * 构建API-INFO对象
     *
     * @return ApiInfo
     */
    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .version("v1.0.1")
                .title("CHENFU-AUTH")
                .description("备注：本系统就是耍着玩的，主要是设计一个不使用框架来完成的用户登录和权限管理模块")
                .termsOfServiceUrl("http://localhost:4883")
                .contact(new Contact("臣服", "https://blog.csdn.net/CHENFU_ZKK", "2671135881@qq.com"))
                .build();
        return apiInfo;
    }

    /**
     * @return
     */
    public List<Parameter> parameters() {
        ArrayList<Parameter> parameters = new ArrayList<>();
        ParameterBuilder tokenBuilder = new ParameterBuilder();
        Parameter token = tokenBuilder.name(Constant.TOKEN).description("登录token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        parameters.add(token);
        return parameters;
    }

    /**
     * @return Docket
     */
    @Bean
    public Docket docket() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .groupName("chenfu-boot")
                .genericModelSubstitutes(ResponseEntity.class)
                .forCodeGeneration(false)
                .useDefaultResponseMessages(true)
                .apiInfo(apiInfo())
                // 选择那些路径和api会生成document
                .select()
                // 对当前包下的API进行监控
                .apis(RequestHandlerSelectors.basePackage("top.chenfu.boot.auth.controller"))
                // 对所有路径进行监控
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(parameters())
                .ignoredParameterTypes(HttpSession.class, HttpServletRequest.class, HttpServletResponse.class);
        return docket;
    }

}
