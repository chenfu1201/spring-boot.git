package top.chenfu.boot.auth.dao;

import tk.mybatis.mapper.common.Mapper;
import top.chenfu.boot.auth.model.Role;

public interface RoleMapper extends Mapper<Role> {
}