package top.chenfu.boot.auth.dao;

import tk.mybatis.mapper.common.Mapper;
import top.chenfu.boot.auth.model.RoleMenu;

public interface RoleMenuMapper extends Mapper<RoleMenu> {
}