package top.chenfu.boot.auth.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.chenfu.boot.auth.model.User;
import top.chenfu.boot.auth.service.UserServiceImpl;
import top.chenfu.boot.auth.util.Constant;
import top.chenfu.boot.auth.util.UserStatusEnum;
import top.chenfu.boot.auth.vo.UserVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping(value = "{id}")
    @ApiOperation(value = "用户详情接口", notes = "用户详情方法")
    @ApiParam(value = "id", name = "用户ID", required = true)
    public ResponseEntity<User> info(@PathVariable(value = "id") Integer id) {
        User user = userService.getUser(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping(value = "add")
    @ApiOperation(value = "用户新增接口", notes = "用户添加方法")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "userName", value = "用户名称", required = true, dataType = "string"),
            @ApiImplicitParam(name = "password", value = "用户密码", required = true, dataType = "string")
    })
    public ResponseEntity add(@RequestBody @Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            String errerMsg = result.getAllErrors().get(0).getDefaultMessage();
            return new ResponseEntity(errerMsg, HttpStatus.BAD_REQUEST);
        }
        Integer index = userService.add(user);
        if (index > 0) {
            return new ResponseEntity<>(user, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(user, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(value = "all")
    @ApiOperation(value = "获取用户列表接口", notes = "获取用户列表方法")
    public ResponseEntity<List<User>> all() {
        List<User> users = userService.selectAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PutMapping(value = "update")
    @ApiOperation(value = "更新用户全部信息接口", notes = "更新用户全部信息方法")
    public ResponseEntity<UserVo> update(@RequestBody @Validated @ApiParam(name = "用户对象", value = "json格式", required = true) UserVo userVo, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            userVo.setMsg(result.getAllErrors().get(0).getDefaultMessage());
            userVo.setCode(400);
            return new ResponseEntity<>(userVo, HttpStatus.BAD_REQUEST);
        }
        Integer i = userService.update(userVo, !UserStatusEnum.IS_PATCH.isStatus());
        if (i > 0) {
            userVo.setCode(200);
            userVo.setMsg("success");
            return new ResponseEntity<>(userVo, HttpStatus.OK);
        }
        return new ResponseEntity<>(userVo, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PatchMapping(value = "update")
    @ApiOperation(value = "更新用户部分信息接口", notes = "更新用户部分信息方法")
    public ResponseEntity<UserVo> updatePatch(@RequestBody @ApiParam(name = "用户对象", value = "json格式", required = true) UserVo userVo) {
        if (Objects.isNull(userVo) || Objects.isNull(userVo.getId())) {
            userVo.setMsg("用户ID不能为null");
            userVo.setCode(400);
            return new ResponseEntity<>(userVo, HttpStatus.BAD_REQUEST);
        }
        Integer i = userService.update(userVo, UserStatusEnum.IS_PATCH.isStatus());
        if (i > 0) {
            userVo.setCode(200);
            userVo.setMsg("success");
            return new ResponseEntity<>(userVo, HttpStatus.OK);
        }
        return new ResponseEntity<>(userVo, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation(value = "删除用户接口", notes = "删除用户方法")
    @ApiParam(value = "id", name = "用户ID", required = true)
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Integer> delete(@PathVariable(value = "id") Integer id) {
        Integer index = userService.delete(id);
        if (index > 0) {
            return new ResponseEntity<>(index, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(value = "token")
    public ResponseEntity<Object> getToken(HttpServletRequest request) {
        Object tokenValue = request.getSession().getAttribute(request.getHeader(Constant.TOKEN));
        System.out.println(JSON.toJSONString(tokenValue));
        return new ResponseEntity<>(tokenValue, HttpStatus.OK);
    }

}
