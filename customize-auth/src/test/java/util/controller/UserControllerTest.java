package util.controller;

import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import top.chenfu.boot.auth.vo.UserVo;

public class UserControllerTest {

    private static final String URI = "http://localhost:4883/user/";

    @Test
    public void info() {
        String url = URI;
        String all = HttpUtil.get(url + 1);
        System.out.println(all);
    }

    @Test
    public void all() {
        String url = URI + "all";
        String users = HttpUtil.get(url);
        System.out.println(users);
    }

    /**
     * 此方法暂时有问题，用swagger调接口即可。
     * 通过查看HttpUtil方法createRequest的源码发现原因如下
     * if (Method.PATCH == method) {
     * this.method = Method.POST;
     * this.header("X-HTTP-Method-Override", "PATCH");
     * } else {
     * this.method = method;
     * }
     * 可知是伪造的Pathch请求，这种方式在楼主当前使用的Spring中行不通，PATCH的Rest请求methond必须得是PATCH
     */
    @Test
    public void update() {
        String url = URI + "update";
        UserVo userVo = new UserVo();
        userVo.setId(2);
        userVo.setUserName("test01");
        userVo.setPassword("123456");
        String body = HttpUtil.createRequest(Method.PATCH, url).body(JSON.toJSONString(userVo)).execute().body();
        System.out.println(body);
    }

    @Test
    public void token() {
        String url = URI + "token";
        UserVo userVo = new UserVo();
        userVo.setId(2);
        userVo.setUserName("test01");
        userVo.setPassword("123456");
        String body = HttpUtil.createRequest(Method.PATCH, url).body(JSON.toJSONString(userVo)).execute().body();
        System.out.println(body);
    }

}
