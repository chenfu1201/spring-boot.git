package util.controller;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import top.chenfu.boot.auth.vo.UserVo;

public class LoginControllerTest {

    private static final String URI = "http://localhost:4883/login";

    @Test
    public void login() {
        String url = URI;
        UserVo userVo = new UserVo();
        userVo.setUserName("test02");
        userVo.setPassword("123456");
        String token = HttpUtil.post(url, JSON.toJSONString(userVo));
        System.out.println(token);
    }
}
