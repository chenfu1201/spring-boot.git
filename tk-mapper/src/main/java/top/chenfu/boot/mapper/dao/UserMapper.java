package top.chenfu.boot.mapper.dao;

import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import top.chenfu.boot.mapper.model.User;

@Repository
public interface UserMapper extends Mapper<User> {
}