package top.chenfu.boot.mapper;

import com.alibaba.fastjson.JSON;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import tk.mybatis.spring.annotation.MapperScan;
import top.chenfu.boot.mapper.dao.UserMapper;
import top.chenfu.boot.mapper.model.User;

import java.util.List;

/**
 * @Auther chenfu
 * @2019/12/17 20:05
 * @Descs
 */
@SpringBootApplication
@MapperScan(value = "top.chenfu.boot.mapper.dao")
public class TkMapperApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext app = SpringApplication.run(TkMapperApp.class, args);
//        UserMapper userMapper = app.getBean(UserMapper.class);
        UserMapper userMapper = (UserMapper) app.getBean("userMapper");
        List<User> users = userMapper.selectAll();
        System.out.println(JSON.toJSONString(users));
    }

}
